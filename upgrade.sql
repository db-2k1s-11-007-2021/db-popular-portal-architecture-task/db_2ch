BEGIN transaction;

ALTER TABLE comment ADD COLUMN date timestamp NOT NULL;

CREATE TABLE status
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE ip_status
(
    id SERIAL PRIMARY KEY,
    ip VARCHAR(19) NOT NULL,
    status_id INT NOT NULL,
    CONSTRAINT ip_fk FOREIGN KEY (ip) REFERENCES "user" (ip) ON DELETE CASCADE,
    CONSTRAINT status_id FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE,
    CONSTRAINT cons_user_status UNIQUE(ip, status_id)
);

CREATE VIEW not_banned_users AS
SELECT
	u.*
FROM "user" u
LEFT JOIN ip_status ips ON u.ip = ips.ip
LEFT JOIN status s ON ips.status_id = s.id
WHERE s.name <> 'ban';

CREATE MATERIALIZED VIEW comments_by_thread as
    SELECT
        t.id,
        COUNT(tc.comment_id)
    FROM thread t
    LEFT JOIN thread_comment tc ON tc.thread_id=t.id
    GROUP BY t.id;

-- REFRESH MATERIALIZED VIEW comments_by_thread;

COMMIT transaction;
