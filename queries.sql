-- Get all threads
SELECT 
    *
FROM thread;


-- Get threads by header or by description
SELECT 
    *
FROM thread
WHERE header = :header OR description = :description;


-- Get threads from not banned users only
SELECT
	*
FROM thread t
LEFT JOIN user_thread ut ON t.id = ut.thread_id
INNER JOIN not_banned_users nbu ON ut.user_id = nbu.id;


-- Get all comments
SELECT
    *
FROM thread t
LEFT JOIN thread_comment tc ON t.id = tc.thread_id 
LEFT JOIN comment c ON tc.comment_id = c.id;


-- Get comments from not banned users only
SELECT
    *
FROM thread t
LEFT JOIN thread_comment tc ON t.id = tc.thread_id 
LEFT JOIN comment c ON tc.comment_id = c.id;
LEFT JOIN user_comment uc ON c.id = uc.comment_id
INNER JOIN not_banned_users nbu ON uc.user_id = nbu.id;


-- Get all comment documents
SELECT
    *
FROM comment c
LEFT JOIN doc_comment dc ON c.id = dc.comment_id
LEFT JOIN doc d ON dc.doc_id = d.id;


-- Get not banned users only
SELECT
    *
FROM not_banned_users;