BEGIN transaction;
-- 1 устанавливет кэшбек
UPDATE client_cashback SET category=’taxi’ WHERE client_id=1;
-- 2 снова хочет установить кэшбек
UPDATE client_cashback SET category=’book’ WHERE client_id=1;

COMMIT;

-- Пояснение
-- При repeatable read во время UPDATE во второй транзакции возникнет lock
-- и активация ежемесячного кешбека произойдет единожды,
-- если использовать read uncommitted или read committed,
-- то будет возможность множественной активации кешбека
