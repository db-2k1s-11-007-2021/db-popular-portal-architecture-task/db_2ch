BEGIN transaction;
ALTER TABLE comment DROP COLUMN date;

DROP TABLE ip_status;
DROP TABLE status;

DROP VIEW not_banned_users;
DROP MATERIALIZED VIEW comments_by_thread;

COMMIT transaction;
