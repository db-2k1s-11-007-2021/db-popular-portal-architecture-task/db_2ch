BEGIN transaction;

CREATE TABLE role
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE "user"
(
    id SERIAL PRIMARY KEY,
    ip VARCHAR(19) UNIQUE NOT NULL,
    role_id INT NOT NULL,
    CONSTRAINT role_id_fg FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE
);

CREATE TABLE thread
(
    id SERIAL PRIMARY KEY,
    header VARCHAR(128) NOT NULL,
    description VARCHAR(1024) NOT NULL
);

-- Пользователи могут искать обсуждения по их названию и по совпадению в описании обсуждения
CREATE INDEX thread_header_idx ON thread (header); -- Тип B-tree, потому что header не уникальный
CREATE INDEX thread_description_idx ON thread (description); -- Тип B-tree, потому что description не уникальный

CREATE TABLE doc
(
    id SERIAL PRIMARY KEY,
    path VARCHAR(255) NOT NULL
);

CREATE TABLE comment
(
    id SERIAL PRIMARY KEY,
    header VARCHAR(128) NOT NULL,
    description VARCHAR(1024) NOT NULL
);

-- Пользователи могут искать комментарии по их названию и по совпадению в описании комменатрия
CREATE INDEX comment_header_idx ON comment (header); -- Тип B-tree, потому что header не уникальный
CREATE INDEX comment_description_idx ON comment (description); -- Тип B-tree, потому что description не уникальный

CREATE TABLE user_thread
(
    id SERIAL PRIMARY KEY,
    user_id INT NOT NULL,
    thread_id INT NOT NULL,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE,
    CONSTRAINT thread_id_fk FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE,
    CONSTRAINT cons_user_thread UNIQUE(user_id, thread_id)
);


CREATE TABLE user_comment
(
    id SERIAL PRIMARY KEY,
    user_id INT NOT NULL,
    comment_id INT NOT NULL,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE,
    CONSTRAINT comment_id_fk FOREIGN KEY (comment_id) REFERENCES comment (id) ON DELETE CASCADE,
    CONSTRAINT cons_user_comment UNIQUE(user_id, comment_id)
);

CREATE TABLE thread_doc
(
    id SERIAL PRIMARY KEY,
    thread_id INT NOT NULL,
    doc_id INT NOT NULL,
    CONSTRAINT thread_id_fk FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE,
    CONSTRAINT doc_id_fk FOREIGN KEY (doc_id) REFERENCES doc (id) ON DELETE CASCADE,
    CONSTRAINT cons_thread_doc UNIQUE(thread_id, doc_id)
);

CREATE TABLE doc_comment
(
    id SERIAL PRIMARY KEY,
    doc_id INT NOT NULL,
    comment_id INT NOT NULL,
    CONSTRAINT doc_id_fk FOREIGN KEY (doc_id) REFERENCES doc (id) ON DELETE CASCADE,
    CONSTRAINT comment_id_fk FOREIGN KEY (comment_id) REFERENCES comment (id) ON DELETE CASCADE,
    CONSTRAINT cons_doc_comment UNIQUE(doc_id, comment_id)
);

CREATE TABLE thread_comment
(
    id SERIAL PRIMARY KEY,
    thread_id INT NOT NULL,
    comment_id INT NOT NULL,
    CONSTRAINT thread_id_fk FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE,
    CONSTRAINT comment_id_fk FOREIGN KEY (comment_id) REFERENCES comment (id) ON DELETE CASCADE,
    CONSTRAINT cons_thread_comment UNIQUE(thread_id, comment_id)
);

COMMIT transaction;
